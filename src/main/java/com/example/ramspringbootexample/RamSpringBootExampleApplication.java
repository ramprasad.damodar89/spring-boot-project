package com.example.ramspringbootexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RamSpringBootExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RamSpringBootExampleApplication.class, args);
	}

}
